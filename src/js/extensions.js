'use strict';
//拓展
var EXT = EXT || {
	
};

//转化为二进制
function toBinary(num, length){
    var length = length || 16;
    var binaryNum = parseInt(num).toString(2);
    var numZeros = length - binaryNum.length;
    var zeros = []
    for (var i = 0; i < numZeros; i++){
        zeros.push(0);
    }
    var strZeros = zeros.join('');
    binaryNum = strZeros + binaryNum;
    return binaryNum;
}

export { EXT }