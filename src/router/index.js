import Vue from 'vue'
import Router from 'vue-router'
const Home = resolve => {
  resolve(require('@/components/home'))
}
Vue.use(Router)
const router = new Router({
  mode: 'history',
  routes:[
    {
      path :'*',//404匹配未找到的页面
      component : Home
    },
    {
      path: '/home',
      name:"home",
      component: Home,
      meta:{
        keepAlive:false
      }
    }
  ]
})
export default router
